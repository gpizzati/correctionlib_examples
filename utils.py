import gc
import json
import sys
import awkward as ak
import uproot
import matplotlib as mpl
mpl.use("Agg")
import mplhep as hep
import matplotlib.pyplot as plt

def set_plot_style():
    d = hep.style.CMS
    plt.style.use([d, hep.style.firamath])

def correctionlib_wrapper(cset):
    def wrapper(*inputs):

        # new_inputs = []
        # num = 0
        # for i in range(len(inputs)):
        #     if isinstance(inputs[i], ak.Array) and inputs[i].ndim > 1:
        #         num = ak.num(inputs[i])
        #         new_inputs.append(ak.flatten(inputs[i]))
        #     else:
        #         new_inputs.append(inputs[i])
        # result = cset.evaluate(*new_inputs)

        # if isinstance(num, ak.Array):
        #     result = ak.unflatten(result, num)
        # else:
        #     result = ak.Array(result)
        # return result

        # new_inputs = []
        # num = 0
        # for i in range(len(inputs)):
        #     if isinstance(inputs[i], ak.Array) and inputs[i].ndim > 1:
        #         num = ak.num(inputs[i])
        #         new_inputs.append(ak.flatten(inputs[i]))
        #     else:
        #         new_inputs.append(inputs[i])
        result = cset.evaluate(*inputs)

        # if isinstance(num, ak.Array):
        #     result = ak.unflatten(result, num)
        # else:
        #     result = ak.Array(result)
        return result

    return wrapper

def fold(h):
    _h = h.copy()
    a = _h.view(True)

    a.value[1, :] = a.value[1, :] + a.value[0, :]
    a.value[0, :] = 0

    a.value[-2, :] = a.value[-2, :] + a.value[-1, :]
    a.value[-1, :] = 0

    a.variance[1, :] = a.variance[1, :] + a.variance[0, :]
    a.variance[0, :] = 0

    a.variance[-2, :] = a.variance[-2, :] + a.variance[-1, :]
    a.variance[-1, :] = 0
    return _h

def read_events(filename, start=0, stop=100, read_form="mc"):
    print("start reading")
    f = uproot.open(filename)
    tree = f["Events"]
    start = min(start, tree.num_entries)
    stop = min(stop, tree.num_entries)
    if start >= stop:
        return ak.Array([])

    branches = [k.name for k in tree.branches]

    events = {}
    with open("data/forms.json", "r") as file:
        forms = json.load(file)
    form = forms[read_form]
    # form = deepcopy(read_form)

    all_branches = []
    for coll in form:
        coll_branches = form[coll]["branches"]
        if len(coll_branches) == 0:
            if coll in branches:
                all_branches.append(coll)
        else:
            for branch in coll_branches:
                branch_name = coll + "_" + branch
                if branch_name in branches:
                    all_branches.append(branch_name)

    events_bad_form = tree.arrays(
        all_branches,
        entry_start=start,
        entry_stop=stop,
        decompression_executor=uproot.source.futures.TrivialExecutor(),
        interpretation_executor=uproot.source.futures.TrivialExecutor(),
    )
    f.close()
    print("success reading file")

    for coll in form:
        d = {}
        coll_branches = form[coll].pop("branches")

        if len(coll_branches) == 0:
            if coll in branches:
                events[coll] = events_bad_form[coll]
            continue

        for branch in coll_branches:
            branch_name = coll + "_" + branch
            if branch_name in branches:
                d[branch] = events_bad_form[branch_name]

        if len(d.keys()) == 0:
            print("did not find anything for", coll, file=sys.stderr)
            continue

        events[coll] = ak.zip(d, **form[coll])
        del d

    print("created events")
    _events = ak.zip(events, depth_limit=1)
    del events
    gc.collect()
    return _events
