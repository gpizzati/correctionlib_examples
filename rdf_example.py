import os
import ROOT
import correctionlib
from data.common import chunk, jec_tag, jet_algo, lvl_compound, jer_tag, jes_uncs

correctionlib.register_pyroot_binding()
ROOT.gInterpreter.Declare('#include "clib_wrapper.cc"')

df = ROOT.RDataFrame(
    "Events",
    chunk["data"]["filenames"][-1],
).Range(chunk["data"]["stop"])

# Load JERC
fname = os.path.abspath("data/clib/jet_jerc.json.gz")
print("\nLoading JSON file: {}".format(fname))
ROOT.gROOT.ProcessLine(
    f'auto cset_jerc = correction::CorrectionSet::from_file("{fname}");'
)

# Load JER
fname = os.path.abspath("data/clib/jer_smear.json.gz")
print("\nLoading JSON file: {}".format(fname))
ROOT.gROOT.ProcessLine(
    f'auto cset_jersmear = correction::CorrectionSet::from_file("{fname}");'
)


key = "{}_{}_{}".format(jec_tag, lvl_compound, jet_algo)
print(key)
ROOT.gROOT.ProcessLine(f'auto sf_cset = cset_jerc->compound().at("{key}");')

df = df.Define(
    "event_random_seed",
    "(int) (1 + event + (luminosityBlock << 10) + (run << 20) + (Jet_eta.size() > 0 ? int(Jet_eta[0]/0.01) : 0))",
)

df = df.Define("pt_gen", "Take(GenJet_pt, Jet_genJetIdx, 0.0f)")
df = df.Define("Jet_pt_raw", "Jet_pt * (1.0 - Jet_rawFactor)")

jet_map = {
    "jet_pt": "Jet_pt",
    "jet_pt_raw": "Jet_pt_raw",
    "jet_eta": "Jet_eta",
    "jet_phi": "Jet_phi",
    "jet_area": "Jet_area",
    "rho": "fixedGridRhoFastjetAll",
    "systematic": "nom",
    "gen_pt": "pt_gen",
    "EventID": "event_random_seed",
}


def wrap_clib_call(cset, *args):
    return "clib_wrapper({" + ",".join(*args) + "}, " + cset + ")"


df = df.Define(
    "sf",
    wrap_clib_call(
        "sf_cset",
        [jet_map[k] for k in ["jet_area", "jet_eta", "jet_pt_raw", "rho"]],
    ),
)
df = df.Redefine("Jet_pt", jet_map["jet_pt_raw"] + " * sf")


for unc in jes_uncs:
    key = f"{jec_tag}_Regrouped_{unc}_{jet_algo}"
    ROOT.gInterpreter.Declare(f'auto sf_unc_cset_{unc} = cset_jerc->at("{key}");')
    df = df.Define(
        f"sf_unc_{unc}",
        wrap_clib_call(
            f"sf_unc_cset_{unc}", [jet_map[k] for k in ["jet_eta", "jet_pt_raw"]]
        ),
    )
    # df = df.Define(f"Jet_pt_{unc}_up", f"(sf + sf_unc_{unc}) * {jet_map["jet_pt_raw"]}")
    # df = df.Define(
    #     f"Jet_pt_{unc}_down", f"(sf - sf_unc_{unc}) * " + jet_map["jet_pt_raw"]
    # )
    df = df.Vary(
        "Jet_pt",
        f'ROOT::RVec<ROOT::RVecF> {{(sf - sf_unc_{unc}) * {jet_map["jet_pt_raw"]}, (sf + sf_unc_{unc}) * {jet_map["jet_pt_raw"]}}}',
        variationTags=["down", "up"],
        variationName=unc,
    )

key = "{}_{}_{}".format(jer_tag, "ScaleFactor", jet_algo)
ROOT.gInterpreter.Declare(f'auto sf_cset_jer = cset_jerc->at("{key}");')

key = "{}_{}_{}".format(jer_tag, "PtResolution", jet_algo)
ROOT.gInterpreter.Declare(f'auto ptres_jer_cset = cset_jerc->at("{key}");')

key = "JERSmear"
ROOT.gInterpreter.Declare(f'auto sf_jersmear_cset = cset_jersmear->at("{key}");')


df = df.Define("tag", '"nom"')
df = df.Vary(
    "tag",
    'ROOT::RVec<std::string>{"down", "up"}',
    variationName="JER",
    variationTags=["down", "up"],
)

for tag, variation_name, jet_pt_name in [
    # ["up", "JER_up", "jet_pt"],
    # ["down", "JER_down", "jet_pt"],
    ["nom", "nom", "jet_pt"],
]:
    df = df.Define(
        "sf_jer",
        wrap_clib_call(
            "sf_cset_jer",
            [
                jet_map["jet_eta"],
                "tag",
                # '"' + tag + '"',
            ],
        ),
    )
    df = df.Define(
        "ptres_jer",
        wrap_clib_call(
            "ptres_jer_cset",
            [
                jet_map["jet_eta"],
                jet_map[jet_pt_name],
                jet_map["rho"],
            ],
        ),
    )

    # add previously obtained JER/JERSF values to inputs
    df = df.Define(
        "sf_jersmear",
        wrap_clib_call(
            "sf_jersmear_cset",
            [
                jet_map[jet_pt_name],
                jet_map["jet_eta"],
                jet_map["gen_pt"],
                jet_map["rho"],
                jet_map["EventID"],
                "ptres_jer",
                "sf_jer",
            ],
        ),
    )

    # Latinos recipe
    # no_jer_mask = (
    #     (jet_map[jet_pt_name] < 50)
    #     & (abs(jet_map["jet_eta"]) >= 2.8)
    #     & (abs(jet_map["jet_eta"]) <= 3.0)
    # )
    df = df.Define(
        "no_jer_mask",
        f'({jet_map[jet_pt_name]}< 50) && ((abs({jet_map["jet_eta"]}) >= 2.8)) && (abs({jet_map["jet_eta"]}) <= 3.0)',
    )

    new_jet_pt_name = jet_pt_name + "_corr"
    if tag != "nom":
        new_jet_pt_name = jet_pt_name + "_" + variation_name

        # df = df.Define(
        #     new_jet_pt_name,
        #     f"Where(no_jer_mask{variation_name}, 1.0f, sf_jersmear{variation_name})*"
        #     + jet_map[jet_pt_name],
        # )
    df = df.Define(
        new_jet_pt_name,
        "Where(no_jer_mask, 1.0f, sf_jersmear)*" + jet_map[jet_pt_name],
    )
    # break


df.Display(
    [
        "Jet_pt",
        "Jet_pt:Absolute_up",
        "jet_pt_JER_up",
        "jet_pt_JER_up",
    ]
).Print()
