chunk = {
    "data": {
        "dataset": "Zjj",
        "filenames": [
            "root://cmsdcadisk.fnal.gov//dcache/uscmsdisk/store/mc/RunIISummer20UL18NanoAODv9/EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole/NANOAODSIM/106X_upgrade2018_realistic_v16_L1v1-v1/40000/12AA1522-B165-FD4F-BE15-96606743AD6C.root",
            "root://dcache-cms-xrootd.desy.de:1094//store/mc/RunIISummer20UL18NanoAODv9/EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole/NANOAODSIM/106X_upgrade2018_realistic_v16_L1v1-v1/40000/12AA1522-B165-FD4F-BE15-96606743AD6C.root",
            "root://ccxrdcms.in2p3.fr:1094//store/mc/RunIISummer20UL18NanoAODv9/EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole/NANOAODSIM/106X_upgrade2018_realistic_v16_L1v1-v1/40000/12AA1522-B165-FD4F-BE15-96606743AD6C.root",
            "root://xrootd-cmst1-door.pic.es:1094//pnfs/pic.es/data/cms/disk/store/mc/RunIISummer20UL18NanoAODv9/EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole/NANOAODSIM/106X_upgrade2018_realistic_v16_L1v1-v1/40000/12AA1522-B165-FD4F-BE15-96606743AD6C.root",
            "root://xrootd-local.unl.edu:1094//store/mc/RunIISummer20UL18NanoAODv9/EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole/NANOAODSIM/106X_upgrade2018_realistic_v16_L1v1-v1/40000/12AA1522-B165-FD4F-BE15-96606743AD6C.root",
            "root://eos.cms.rcac.purdue.edu//store/mc/RunIISummer20UL18NanoAODv9/EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole/NANOAODSIM/106X_upgrade2018_realistic_v16_L1v1-v1/40000/12AA1522-B165-FD4F-BE15-96606743AD6C.root",
        ],
        "start": 0,
        "stop": 100_000,
        "read_form": "mc",
    },
    "error": "",
    "result": {},
    "priority": 0,
    "weight": 8,
}

# JEC base tag
jec_tag = "Summer19UL18_V5_MC"
# jet algorithms
jet_algo = "AK4PFchs"
# jet energy correction level
lvl_compound = "L1L2L3Res"
# https://twiki.cern.ch/twiki/bin/viewauth/CMS/JECUncertaintySources#Run_2_reduced_set_of_uncertainty
jes_uncs = [
    "Absolute",
    "Absolute_2018",
    "BBEC1",
    "BBEC1_2018",
    "EC2",
    "EC2_2018",
    "FlavorQCD",
    "HF",
    "HF_2018",
    "RelativeBal",
    "RelativeSample_2018",
]

jer_tag = "Summer19UL18_JRV2_MC"