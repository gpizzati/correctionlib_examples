#ifndef macro
#define macro
#include <algorithm>
#include <cstdint>
#include <memory>
#include <stdexcept>
#include <string>
#include <variant>
#include <vector>
#include "ROOT/RVec.hxx"

template <class... Ts>
struct overloaded : Ts...
{
    using Ts::operator()...;
};
template <class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;

// namespace ROOT
// {
//     using RVecF = std::vector<float>;
//     using RVecI = std::vector<int>;
//     using RVecD = std::vector<double>;
// } // namespace ROOT
using correctionlib_variant = std::variant<int, double, std::string>;
using args_variant = std::variant<ROOT::RVecI, ROOT::RVecF, ROOT::RVecD, int,
                                  float, double, std::string>;

// namespace correction
// {
//     struct CompoundCorrection
//     {
//         double evaluate(const std::vector<correctionlib_variant> &vals) const;
//     };
// } // namespace correction

decltype(auto) get_collection_length_with_checks(
    const std::vector<args_variant> &values)
{
    auto condition_lambda = [](const args_variant &val)
    {
        return std::holds_alternative<ROOT::RVecI>(val) ||
               std::holds_alternative<ROOT::RVecF>(val) ||
               std::holds_alternative<ROOT::RVecD>(val);
    };
    auto it = std::find_if(values.begin(), values.end(), condition_lambda);
    if (it == values.end())
        return std::size_t(1);

    auto reference_size =
        std::visit(overloaded{
                       [](const auto &vec)
                       { return vec.size(); },
                       [](int arg)
                       { return std::size_t(1); },
                       [](double arg)
                       { return std::size_t(1); },
                       [](float arg)
                       { return std::size_t(1); },
                       [](const std::string &arg)
                       { return std::size_t(1); },
                   },
                   *it);

    if (!std::all_of(values.begin(), values.end(),
                     [&reference_size](const args_variant &val)
                     {
                         return std::visit(
                             overloaded{
                                 [&reference_size](auto &&vec)
                                 {
                                     return vec.size() == reference_size;
                                 },
                                 [](int arg)
                                 { return true; },
                                 [](double arg)
                                 { return true; },
                                 [](float arg)
                                 { return true; },
                                 [](const std::string &arg)
                                 { return true; },
                             },
                             val);
                     }))
        throw std::invalid_argument("Not all vector inputs have same length.");

    return reference_size;
}

template <typename T>
ROOT::RVecF clib_wrapper(
    const std::vector<args_variant> &values,
    std::shared_ptr<const T> cset)
{
    auto collection_len = get_collection_length_with_checks(values);

    ROOT::RVecF results(collection_len, 1.f);
    for (int icoll = 0; icoll < collection_len; icoll++)
    {
        std::vector<correctionlib_variant> single_values(values.size());
        std::generate(
            single_values.begin(), single_values.end(),
            [&values, icoll, n = 0]() mutable -> correctionlib_variant
            {
                const auto &var = values[n];
                n++;
                return std::visit(overloaded{
                                      [icoll](const auto &vec) -> correctionlib_variant
                                      { return vec[icoll]; },
                                      [](int arg) -> correctionlib_variant
                                      { return arg; },
                                      [](double arg) -> correctionlib_variant
                                      { return arg; },
                                      [](float arg) -> correctionlib_variant
                                      { return arg; },
                                      [](const std::string &arg) -> correctionlib_variant
                                      { return arg; },
                                  },
                                  var);
            });

        results[icoll] = cset->evaluate(single_values);
    }

    return results;
}

#endif